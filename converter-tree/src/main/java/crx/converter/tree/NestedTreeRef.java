/* ----------------------------------------------------------------------------
 * This file is part of CCoPI-Mono Converter Engine.  
 * 
 * Copyright (C) 2016 by the following organisation(s):- 
 * 1. Cyprotex Discovery Ltd, Macclesfield, England, UK
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation. A copy of the license agreement is provided
 * in the file named "LICENSE.txt" included with this software distribution.
 * ----------------------------------------------------------------------------
 */

package crx.converter.tree;

/**
 * A nested tree reference.
 */
public class NestedTreeRef {
	/**
	 * Binary reference of the model element. 
	 */
	public BinaryTree bt = null;
	
	/**
	 * Model element associated with a nested tree reference.
	 */
	public Object element = null;
	
	/**
	 * Constructor
	 * @param element_ Model element
	 * @param bt_ Binary tree representing the model element.
	 */
	public NestedTreeRef(Object element_, BinaryTree bt_) {
		if (element_ == null) throw new NullPointerException("The element is NULL.");
		if (bt_ == null) throw new NullPointerException("The binary tree is NULL.");
		
		element = element_;
		bt = bt_;
	}
	
	@Override
	public String toString() {
		String format = "element=%s, Tree=%s";
		return String.format(format, element, bt);
	}
}
