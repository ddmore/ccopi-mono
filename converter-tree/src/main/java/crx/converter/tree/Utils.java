/* ----------------------------------------------------------------------------
 * This file is part of CCoPI-Mono Converter Engine.  
 * 
 * Copyright (C) 2016 by the following organisation(s):- 
 * 1. Cyprotex Discovery Ltd, Macclesfield, England, UK
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation. A copy of the license agreement is provided
 * in the file named "LICENSE.txt" included with this software distribution.
 * ----------------------------------------------------------------------------
 */

package crx.converter.tree;

/**
 * Class utility functions.
 */
public abstract class Utils {
	/**
	 * Get the class name of an object.
	 * @param o Instance Variable
	 * @return java.lang.String
	 */
	public static String getClassName(Object o) {
		if (o == null) return null;
		
		Class<?> c = o.getClass();
		String FQClassName = c.getName();
		int firstChar;
		firstChar = FQClassName.lastIndexOf ('.') + 1;
		if ( firstChar > 0 ) {
			FQClassName = FQClassName.substring ( firstChar );
		}
		return FQClassName;
	}
	
	/**
	 * Get the class name of an object.
	 * @param o
	 * @return java.lang.String
	 */
	public static String getPackageName(Object o) {
		if (o == null) return "null";
		
		Class<?> c = o.getClass();
		String fullyQualifiedName = c.getName();
		int lastDot = fullyQualifiedName.lastIndexOf ('.');
		if (lastDot==-1){ return ""; }
		return fullyQualifiedName.substring (0, lastDot);
	}
}
