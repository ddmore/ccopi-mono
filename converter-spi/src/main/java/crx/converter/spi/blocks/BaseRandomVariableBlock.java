/* ----------------------------------------------------------------------------
 * This file is part of CCoPI-Mono Converter Engine.  
 * 
 * Copyright (C) 2016 by the following organisation(s):- 
 * 1. Cyprotex Discovery Ltd, Macclesfield, England, UK
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation. A copy of the license agreement is provided
 * in the file named "LICENSE.txt" included with this software distribution.
 * ----------------------------------------------------------------------------
 */

package crx.converter.spi.blocks;

import java.util.List;

import crx.converter.engine.common.CorrelationRef;
import eu.ddmore.libpharmml.dom.modeldefn.ParameterRandomVariable;

/**
 * Base code block that contains random variables.
 */
public interface BaseRandomVariableBlock extends IndividualParameterBlock {
	/**
	 * Get a list of correlation references.
	 * @return java.util.List<CorrelationRef>
	 */
	public List<CorrelationRef> getCorrelations();
	
	/**
	 * A list of pairwise linked variables
	 * @return java.util.List<eu.ddmore.libpharmml.dom.modeldefn.ParameterRandomVariable>
	 */
	public List<ParameterRandomVariable> getLinkedRandomVariables();
}
