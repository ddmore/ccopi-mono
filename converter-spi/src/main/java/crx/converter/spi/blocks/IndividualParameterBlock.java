/* ----------------------------------------------------------------------------
 * This file is part of CCoPI-Mono Converter Engine.  
 * 
 * Copyright (C) 2016 by the following organisation(s):- 
 * 1. Cyprotex Discovery Ltd, Macclesfield, England, UK
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation. A copy of the license agreement is provided
 * in the file named "LICENSE.txt" included with this software distribution.
 * ----------------------------------------------------------------------------
 */

package crx.converter.spi.blocks;

import java.util.List;

import crx.converter.engine.Part;
import eu.ddmore.libpharmml.dom.modeldefn.IndividualParameter;

/**
 * A block of individual parameters.
 *
 */
public interface IndividualParameterBlock extends Part {
	/**
	 * Add an individual parameters to the block
	 * @param ip Individual parameter
	 * @return boolean
	 */
	public boolean addIndividualParameter(IndividualParameter ip);
	
	/**
	 * The the list of individual parameters.
	 * @return java.util.List<IndividualParameter>
	 */
	public List<IndividualParameter> getIndividualParameters();
	
	/**
	 * Flag whether the individual parameter block is empty.
	 * @return boolean
	 */
	public boolean hasIndividualParameters();
}
