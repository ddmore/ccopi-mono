/* ----------------------------------------------------------------------------
 * This file is part of CCoPI-Mono Converter Engine.  
 * 
 * Copyright (C) 2016 by the following organisation(s):- 
 * 1. Cyprotex Discovery Ltd, Macclesfield, England, UK
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation. A copy of the license agreement is provided
 * in the file named "LICENSE.txt" included with this software distribution.
 * ----------------------------------------------------------------------------
 */

package crx.converter.spi;

import java.util.List;

import crx.converter.spi.steps.OptimalDesignStep_;
import eu.ddmore.libpharmml.dom.commontypes.Symbol;

/**
 * Common methods required by an Optimal Design (OD) converter.
 */
public interface OptimalDesignLexer extends ILexer {
	/**
	 * Get a list of model elements listed in the declared error model outputs.
	 * @return List<Symbol>
	 */
	public List<Symbol> getContinuousOutputs();
	
	/**
	 * Get the lexed OD step bound to an PharmML model.
	 * @return OptimalDesignStep_
	 */
	public OptimalDesignStep_ getOptimalDesignStep();
}
