/* ----------------------------------------------------------------------------
 * This file is part of CCoPI-Mono Converter Engine.  
 * 
 * Copyright (C) 2016 by the following organisation(s):- 
 * 1. Cyprotex Discovery Ltd, Macclesfield, England, UK
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation. A copy of the license agreement is provided
 * in the file named "LICENSE.txt" included with this software distribution.
 * ----------------------------------------------------------------------------
 */

package crx.converter.engine.common;

import crx.converter.spi.ILexer;
import eu.ddmore.libpharmml.dom.dataset.ColumnDefinition;
import eu.ddmore.libpharmml.dom.maths.Piece;

/**
 * Dose event definition whose mapping to a model is defined by a value read from a flat file.
 */
public class ConditionalDoseEvent extends InputColumnEvent {
	public ConditionalDoseEvent(ILexer lexer, ColumnDefinition col_, Piece piece) { 
		super(lexer, col_, piece);
	}
}
