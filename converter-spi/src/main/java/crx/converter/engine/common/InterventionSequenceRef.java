/* ----------------------------------------------------------------------------
 * This file is part of CCoPI-Mono Converter Engine.  
 * 
 * Copyright (C) 2016 by the following organisation(s):- 
 * 1. Cyprotex Discovery Ltd, Macclesfield, England, UK
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation. A copy of the license agreement is provided
 * in the file named "LICENSE.txt" included with this software distribution.
 * ----------------------------------------------------------------------------
 */

package crx.converter.engine.common;

import eu.ddmore.libpharmml.dom.commontypes.OidRef;

/**
 * Reference to an intervention sequence.
 */
public class InterventionSequenceRef {
	/**
	 * Administration identifier
	 */
	public String administration_oid = null;
	
	/**
	 * Intervention start time.
	 */
	public double start = 0.0;
	
	/**
	 * Constructor
	 * @param admin_ref OID of an Administration
	 * @param start_ Sequence Start Time
	 */
	public InterventionSequenceRef(OidRef admin_ref, double start_) {
		if (admin_ref == null) throw new NullPointerException("OID Reference is NULL");
		administration_oid = admin_ref.getOidRef();
		if (administration_oid == null) throw new NullPointerException("OID is NULL");
		start = start_;
	}
}
