/* ----------------------------------------------------------------------------
 * This file is part of CCoPI-Mono Converter Engine.  
 * 
 * Copyright (C) 2016 by the following organisation(s):- 
 * 1. Cyprotex Discovery Ltd, Macclesfield, England, UK
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation. A copy of the license agreement is provided
 * in the file named "LICENSE.txt" included with this software distribution.
 * ----------------------------------------------------------------------------
 */

package crx.converter.engine.common;

import eu.ddmore.libpharmml.dom.commontypes.PharmMLElement;
import eu.ddmore.libpharmml.dom.modeldefn.CovariateDefinition;

/**
 * Optimal design class to associate a proportions array/vector to a covariate.
 */
public class CategoryProportions {
	public CovariateDefinition cov = null;
	public PharmMLElement proportions = null;
	
	public CategoryProportions(CovariateDefinition cov_, PharmMLElement proportions_) {
		if (cov_ == null) throw new NullPointerException("Covariate is NULL");
		if (proportions_ == null) throw new NullPointerException("Proportions is NULL");
		
		cov = cov_;
		proportions = proportions_;
	}

	/**
	 * Get the covariate.
	 * @return CovariateDefinition
	 */
	public CovariateDefinition getCovariate() { return cov; }

	/**
	 * Get the proportions variable.
	 * @return PharmMLElement
	 */
	public PharmMLElement getProportions() { return proportions; }
}
