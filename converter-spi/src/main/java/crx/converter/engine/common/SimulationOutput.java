/* ----------------------------------------------------------------------------
 * This file is part of CCoPI-Mono Converter Engine.  
 * 
 * Copyright (C) 2016 by the following organisation(s):- 
 * 1. Cyprotex Discovery Ltd, Macclesfield, England, UK
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation. A copy of the license agreement is provided
 * in the file named "LICENSE.txt" included with this software distribution.
 * ----------------------------------------------------------------------------
 */

package crx.converter.engine.common;

import eu.ddmore.libpharmml.dom.commontypes.PharmMLRootType;

/**
 * Simulation output element.
 */
public class SimulationOutput {
	public PharmMLRootType v = null;
	
	public SimulationOutput(PharmMLRootType v_) {
		if (v_ == null) throw new NullPointerException("Error model variable reference is NULL.");
		v = v_;
	}
}
