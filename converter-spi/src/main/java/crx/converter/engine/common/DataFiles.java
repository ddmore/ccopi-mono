/* ----------------------------------------------------------------------------
 * This file is part of CCoPI-Mono Converter Engine.  
 * 
 * Copyright (C) 2016 by the following organisation(s):- 
 * 1. Cyprotex Discovery Ltd, Macclesfield, England, UK
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation. A copy of the license agreement is provided
 * in the file named "LICENSE.txt" included with this software distribution.
 * ----------------------------------------------------------------------------
 */

package crx.converter.engine.common;

import java.util.ArrayList;
import java.util.List;

import eu.ddmore.libpharmml.dom.trialdesign.ExternalDataSet;

/**
 * A container class designed to create a list of data files associated with a PharmML model.
 * 
 */
public class DataFiles {
	private List<ExternalDataSet> externalDataSets = new ArrayList<ExternalDataSet>();
	
	/**
	 * Get a list of External data set references as read from the PharmML model.
	 * @return java.util.List<eu.ddmore.libpharmml.dom.modellingsteps.ExternalDataSet>
	 */
	public List<ExternalDataSet> getExternalDataSets() {
		return externalDataSets;
	}
	
	/**
	 * Set a list of External data set references as read from the PharmML model.
	 */
	public void setExternalDataSets(List<ExternalDataSet> externalDataSets_) {
		externalDataSets = externalDataSets_;
	}
}
