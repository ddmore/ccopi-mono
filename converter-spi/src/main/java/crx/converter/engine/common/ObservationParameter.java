/* ----------------------------------------------------------------------------
 * This file is part of CCoPI-Mono Converter Engine.  
 * 
 * Copyright (C) 2016 by the following organisation(s):- 
 * 1. Cyprotex Discovery Ltd, Macclesfield, England, UK
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation. A copy of the license agreement is provided
 * in the file named "LICENSE.txt" included with this software distribution.
 * ----------------------------------------------------------------------------
 */

package crx.converter.engine.common;

import crx.converter.spi.blocks.ObservationBlock;
import eu.ddmore.libpharmml.dom.commontypes.PharmMLRootType;
import eu.ddmore.libpharmml.dom.modeldefn.PopulationParameter;

/**
 * A class that flags a parameter has observation model scope only.<br/>
 * Simple a wrapper class so that a parameter declaration does not end up in the
 * global parameter array.
 */
public class ObservationParameter extends PharmMLRootType {
	public PopulationParameter param = null;
	public ObservationBlock parent = null;
	
	/**
	 * Constructor
	 * @param parent_ Parent Block
	 * @param param_ Observation model scoped parameter
	 */
	public ObservationParameter(ObservationBlock parent_, PopulationParameter param_) {
		if (parent_ == null || param_ == null) throw new NullPointerException("A parameter reference argument cannot be NULL.");
		
		parent = parent_;
		param = param_;
	}
	
	/**
	 * Variable name in a coding context.
	 * @return String
	 */
	public String getName() {
		String format = "%s_%s";	
		return String.format(format, parent.getName(), param.getSymbId());
	}
	
	/**
	 * Get the referenced population parameter.
	 * @return PopulationParameter
	 */
	public PopulationParameter getPopulationParameter() { return param; }
}
