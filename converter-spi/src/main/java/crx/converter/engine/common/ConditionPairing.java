/* ----------------------------------------------------------------------------
 * This file is part of CCoPI-Mono Converter Engine.  
 * 
 * Copyright (C) 2016 by the following organisation(s):- 
 * 1. Cyprotex Discovery Ltd, Macclesfield, England, UK
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation. A copy of the license agreement is provided
 * in the file named "LICENSE.txt" included with this software distribution.
 * ----------------------------------------------------------------------------
 */

package crx.converter.engine.common;

import eu.ddmore.libpharmml.dom.commontypes.PharmMLElement;

/**
 * Condition pairing as defined by a probability assignment.
 */
public class ConditionPairing {
	public PharmMLElement lhs = null, rhs = null;
	
	public ConditionPairing(PharmMLElement lhs_, PharmMLElement rhs_) {
		if (lhs_ == null) throw new NullPointerException("Category element is NULL");
		if (rhs_ == null) throw new NullPointerException("Condition element is NULL");
		
		lhs = lhs_;
		rhs = rhs_;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		
		sb.append("[lhs=");
		if (lhs != null) sb.append(lhs);
		sb.append(",");
		
		sb.append("rhs=");
		if (rhs != null) sb.append(rhs);
		sb.append("]");
		
		return sb.toString();
	}
}
