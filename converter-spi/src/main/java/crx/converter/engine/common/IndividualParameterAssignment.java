/* ----------------------------------------------------------------------------
 * This file is part of CCoPI-Mono Converter Engine.  
 * 
 * Copyright (C) 2016 by the following organisation(s):- 
 * 1. Cyprotex Discovery Ltd, Macclesfield, England, UK
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation. A copy of the license agreement is provided
 * in the file named "LICENSE.txt" included with this software distribution.
 * ----------------------------------------------------------------------------
 */

package crx.converter.engine.common;

import eu.ddmore.libpharmml.dom.modeldefn.IndividualParameter;

/**
 * Class wrapping an individual parameter assignment within a trial design replicate loop.<br/>
 * Class differentiates an random variable assignment block from a reference in an equation.
 */
public class IndividualParameterAssignment {
	/**
	 * Parameter
	 */
	public IndividualParameter parameter = null;
	
	/**
	 * Constructor
	 * @param parameter_ Parameter
	 */
	public IndividualParameterAssignment(IndividualParameter parameter_) {
		parameter = parameter_;
	}
}
