/* ----------------------------------------------------------------------------
 * This file is part of CCoPI-Mono Converter Engine.  
 * 
 * Copyright (C) 2016 by the following organisation(s):- 
 * 1. Cyprotex Discovery Ltd, Macclesfield, England, UK
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation. A copy of the license agreement is provided
 * in the file named "LICENSE.txt" included with this software distribution.
 * ----------------------------------------------------------------------------
 */

package crx.converter.engine.common;

import eu.ddmore.libpharmml.dom.modellingsteps.ParameterEstimate;

/**
 * Class wrap a simple parameter assignment from an estimation.<br/>
 * Dual-problem assignment class.
 */
public class ParameterAssignmentFromEstimation {
	public ParameterEstimate p = null;
	
	public ParameterAssignmentFromEstimation(ParameterEstimate p_) {
		if (p_ == null) throw new NullPointerException();
		p = p_;
	}
}
