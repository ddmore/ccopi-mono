/* ----------------------------------------------------------------------------
 * This file is part of CCoPI-Mono Converter Engine.  
 * 
 * Copyright (C) 2016 by the following organisation(s):- 
 * 1. Cyprotex Discovery Ltd, Macclesfield, England, UK
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation. A copy of the license agreement is provided
 * in the file named "LICENSE.txt" included with this software distribution.
 * ----------------------------------------------------------------------------
 */

package crx.converter.engine.common;

import eu.ddmore.libpharmml.dom.commontypes.SymbolRef;

/**
 * A symbol reference for a conditional dose entry read from an external file.
 */
public class ConditionalDoseEventRef extends SymbolRef {
	private ConditionalDoseEvent evt = null;
	private Object value = null;
	
	/**
	 * Constructor
	 * @param evt_	Conditional Dose Event
	 * @param symbId_ Column Definition
	 */
	public ConditionalDoseEventRef(ConditionalDoseEvent evt_, String symbId_) {
		super();
		
		if (evt_ == null) throw new NullPointerException("Event definition is NULL");
		if (symbId_ == null) throw new NullPointerException("Symbol identifier is NULL");
		
		evt = evt_;
		setSymbIdRef(symbId_);
	}
	
	/**
	 * Get the dose event definition
	 * @return ConditionalDoseEvent
	 */
	public ConditionalDoseEvent getEvent() { return evt; }
	
	/**
	 * Assign whatever value (number, string, level) that can be read from a quantity 
	 * read from an external flat file.
	 * @return java.lang.Object
	 */
	public Object getValue() { return value ; }
	
	/**
	 * Assign whatever value (number, string, level) that can be read from a quantity 
	 * read from an external flat file.
	 */
	public void setValue(Object value_) { value = value_; }
}
