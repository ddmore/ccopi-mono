/* ----------------------------------------------------------------------------
 * This file is part of CCoPI-Mono Converter Engine.  
 * 
 * Copyright (C) 2016 by the following organisation(s):- 
 * 1. Cyprotex Discovery Ltd, Macclesfield, England, UK
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation. A copy of the license agreement is provided
 * in the file named "LICENSE.txt" included with this software distribution.
 * ----------------------------------------------------------------------------
 */

package crx.converter.engine.scriptlets;

import crx.converter.engine.BaseEngine;

/**
 * Name of a Python utility function definition that can be included in a scriptlet.
 */
public enum PyFunctionName {
	LINSPACE("linspace"),
	MAX(BaseEngine.MAX.toLowerCase()),
	MIN(BaseEngine.MIN.toLowerCase()),
	SORT("sorted");
	
	/**
	 * Create enumeration value from a string.
	 * @param v
	 * @return FunctionDefinitionRef
	 */
	public static PyFunctionName fromValue(String v) {
        for (PyFunctionName c: PyFunctionName.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
	
	private final String value;
	
	private PyFunctionName(String v) { value = v; }
	
    @Override
    public String toString() { return value; }
}