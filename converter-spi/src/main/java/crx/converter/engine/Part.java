/* ----------------------------------------------------------------------------
 * This file is part of CCoPI-Mono Converter Engine.  
 * 
 * Copyright (C) 2016 by the following organisation(s):- 
 * 1. Cyprotex Discovery Ltd, Macclesfield, England, UK
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation. A copy of the license agreement is provided
 * in the file named "LICENSE.txt" included with this software distribution.
 * ----------------------------------------------------------------------------
 */

package crx.converter.engine;

import java.util.List;

/**
 * A part represents a block of code to be created from elements in a PharmMl model.
 *
 */
public interface Part {
	/**
	 * Method to build the syntax trees bound to a part.
	 */
	public void buildTrees();
	
	/**
	 * Get the name of the code block
	 * @return java.lang.String
	 */
	public String getName();
	
	/**
	 * Get a list of symbols bound to a part.
	 * @return java.util.List<java.lang.String>
	 */
	abstract public List<String> getSymbolIds();
	
	/**
	 * Check if the Part has a bound model symbol.
	 * @param name Name of a variable
	 * @return boolean
	 */
	abstract public boolean hasSymbolId(String name);
}
