/* ----------------------------------------------------------------------------
 * This file is part of CCoPI-Mono Converter Engine.  
 * 
 * Copyright (C) 2016 by the following organisation(s):- 
 * 1. Cyprotex Discovery Ltd, Macclesfield, England, UK
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation. A copy of the license agreement is provided
 * in the file named "LICENSE.txt" included with this software distribution.
 * ----------------------------------------------------------------------------
 */

package crx.converter.engine;

import eu.ddmore.libpharmml.dom.commontypes.SymbolRef;

/**
 * A category reference in a fixed effect statement.
 */
public class FixedEffectCategoryRef {
	public String catId = null;
	public SymbolRef cov = null; 
	public SymbolRef coeff = null;
	
	/**
	 * Constructor
	 * @param cov_ Covariate Nae
	 * @param catId_ Category Identifier
	 */
	public FixedEffectCategoryRef(SymbolRef cov_, SymbolRef coeff_, String catId_) {
		if (cov_ == null) throw new NullPointerException("The covariate cannot be NULL");
		if (coeff_ == null) throw new NullPointerException("The coefficient cannot be NULL");
		if (catId_ == null) throw new NullPointerException("The category identifier cannot be NULL");
		
		cov = cov_;
		catId = catId_;
		coeff = coeff_;
	}
}