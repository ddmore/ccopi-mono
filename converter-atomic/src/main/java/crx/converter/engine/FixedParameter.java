/* ----------------------------------------------------------------------------
 * This file is part of CCoPI-Mono Converter Engine.  
 * 
 * Copyright (C) 2016 by the following organisation(s):- 
 * 1. Cyprotex Discovery Ltd, Macclesfield, England, UK
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation. A copy of the license agreement is provided
 * in the file named "LICENSE.txt" included with this software distribution.
 * ----------------------------------------------------------------------------
 */

package crx.converter.engine;

import eu.ddmore.libpharmml.dom.modellingsteps.ParameterEstimate;

/**
 * A fixed parameter in an estimation.
 */
public class FixedParameter {
	public ParameterEstimate pe = null;
	
	/**
	 * Constructor
	 * @param p_ Parameter Estimate source
	 */
	public FixedParameter(ParameterEstimate p_) {
		if (p_ == null) throw new NullPointerException();
		pe = p_;
	}
}
