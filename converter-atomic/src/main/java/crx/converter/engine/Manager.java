/* ----------------------------------------------------------------------------
 * This file is part of CCoPI-Mono Converter Engine.  
 * 
 * Copyright (C) 2016 by the following organisation(s):- 
 * 1. Cyprotex Discovery Ltd, Macclesfield, England, UK
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation. A copy of the license agreement is provided
 * in the file named "LICENSE.txt" included with this software distribution.
 * ----------------------------------------------------------------------------
 */

package crx.converter.engine;

/**
 * Simple class to supply numeric identifiers to a converter.
 */
public class Manager {
	private boolean fixedRunId = false;
	private long run_id = 0;
	
	/**
	 * Convenience method to create a run identifier.
	 * @return java.lang.String
	 */
	public String generateRunId() {
		if (!fixedRunId) {
			String id = "run_id_" + Long.toString(run_id);
			run_id++;
			return id;
		} else {
			return "run";
		}
	}
	
	/**
	 * Flag whether the manager is using a fixed run identifier.
	 * @return boolean
	 */
	public boolean isFixedRunId() {
		return fixedRunId; 
	}
	
	/**
	 * Resets the run identifier back to zero.
	 */
	public void resetRunId() {
		run_id = 0;
	}
	
	/**
	 * Create identifiers with a numeric component.
	 * @param fixedRunId_ Decision
	 */
	public void setFixedRunId(boolean fixedRunId_) {
		fixedRunId = fixedRunId_;
	}
	
	/**
	 * Set the number start generating identifiers.
	 * @param run_id_ Number
	 */
	public void setRunId(long run_id_) { run_id = run_id_; }
}
