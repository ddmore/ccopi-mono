/* ----------------------------------------------------------------------------
 * This file is part of CCoPI-Mono Converter Engine.  
 * 
 * Copyright (C) 2016 by the following organisation(s):- 
 * 1. Cyprotex Discovery Ltd, Macclesfield, England, UK
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation. A copy of the license agreement is provided
 * in the file named "LICENSE.txt" included with this software distribution.
 * ----------------------------------------------------------------------------
 */

package crx.converter.engine;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import eu.ddmore.convertertoolbox.api.response.ConversionDetail;

/**
 * Conversion detail class.
 */
public class ConversionDetail_ implements ConversionDetail {
	private File file = null;
	private Map<String, String> info = new HashMap<String, String>();
	private Severity severity = Severity.INFO;
	private String message = null;
	
	@Override
	public void addInfo(String key, String value) {
		if (info != null) info.put(key, value);
	}
	
	@Override
	public Map<String, String> getInfo() {
		return info;
	}
	
	@Override
	public String getMessage() {
		return message;
	}
	
	@Override
	public Severity getServerity() {
		return severity;
	}
	
	@Override
	public void setInfo(Map<String, String> info_) {
		info = info_; 	
	}
	
	@Override
	public void setMessage(String msg) {
		if (msg != null) message = msg;
	}
	
	@Override
	public void setSeverity(Severity s) {
		severity = s;
	}
	
	public File getFile() {
		return file;
	}

	public void setFile(File value) {
		file = value;
	}
}
